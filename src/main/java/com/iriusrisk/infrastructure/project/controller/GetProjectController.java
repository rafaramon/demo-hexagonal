package com.iriusrisk.infrastructure.project.controller;

import com.iriusrisk.application.project.ProjectGetter;
import com.iriusrisk.domain.project.Project;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public final class GetProjectController {

    private final ProjectGetter projectGetter;

    public GetProjectController(ProjectGetter projectGetter) {
        this.projectGetter = projectGetter;
    }

    @GetMapping("/projects/{uuid}")
    public ResponseEntity<Project> getProjectByUuid(@PathVariable String uuid) {
        return new ResponseEntity<>(projectGetter.getProjectByUuid(uuid), HttpStatus.OK);
    }
}
