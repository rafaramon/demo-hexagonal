package com.iriusrisk.infrastructure.project.repository.inmemory;

import com.iriusrisk.domain.project.Project;
import com.iriusrisk.domain.project.repository.ProjectQueryRepository;

import java.util.List;

public class ProjectInMemoryQueryRepository implements ProjectQueryRepository {

    @Override
    public Project findByUuid(String uuid) {
        return null;
    }

    @Override
    public List<Project> findAll() {
        return null;
    }
}
