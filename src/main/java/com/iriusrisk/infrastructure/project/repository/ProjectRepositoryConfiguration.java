package com.iriusrisk.infrastructure.project.repository;

import com.iriusrisk.domain.project.repository.ProjectQueryRepository;
import com.iriusrisk.domain.project.repository.ProjectRepository;
import com.iriusrisk.infrastructure.project.repository.inmemory.ProjectInMemoryQueryRepository;
import com.iriusrisk.infrastructure.project.repository.inmemory.ProjectInMemoryRepository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectRepositoryConfiguration {

    // We can use a configuration class or whatever to define which implementation to use
    // THIS IS ONLY AN EXAMPLE

    @Bean
    public ProjectRepository getProjectRepository() {
        return new ProjectInMemoryRepository();
    }

    @Bean
    public ProjectQueryRepository getProjectQueryRepository() {
        return new ProjectInMemoryQueryRepository();
    }
}
