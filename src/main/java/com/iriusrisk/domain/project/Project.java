package com.iriusrisk.domain.project;

public final class Project {
    private final String uuid;
    private final String ref;
    private final String name;
    private final String description;

    public Project(String uuid, String ref, String name, String description) {
        this.uuid = uuid;
        this.ref = ref;
        this.name = name;
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public String getRef() {
        return ref;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
