package com.iriusrisk.domain.project.repository;

import com.iriusrisk.domain.project.Project;

public interface ProjectRepository {
    void save(Project project);
}
