package com.iriusrisk.domain.project.repository;

import com.iriusrisk.domain.project.Project;

import java.util.List;

public interface ProjectQueryRepository {
    Project findByUuid(String uuid);

    List<Project> findAll();

}
