package com.iriusrisk.application.project;

import com.iriusrisk.domain.project.Project;
import com.iriusrisk.domain.project.repository.ProjectQueryRepository;
import com.iriusrisk.domain.project.repository.ProjectRepository;

import org.springframework.stereotype.Component;

@Component
public class ProjectGetter {

    private final ProjectQueryRepository projectQueryRepository;

    public ProjectGetter(ProjectQueryRepository projectQueryRepository) {
        this.projectQueryRepository = projectQueryRepository;
    }

    public Project getProjectByUuid(String uuid) {
        return projectQueryRepository.findByUuid(uuid);
    }
}
